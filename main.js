import './hacks.js';
import * as THREE from 'three';
import * as itowns from 'itowns';
import proj4 from 'proj4';
import { OrbitControls } from 'three/addons/controls/OrbitControls';
import { OBJLoader } from 'three/addons/loaders/OBJLoader.js';
import {MTLLoader} from 'three/addons/loaders/MTLLoader.js';


// Define crs projection that we will use (taken from https://epsg.io/3946, Proj4js section)
proj4.defs('EPSG:3946', '+proj=lcc +lat_1=45.25 +lat_2=46.75 +lat_0=46 +lon_0=3 +x_0=1700000 +y_0=5200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs');

var extent;
var viewerDiv;
var view;

// Define geographic extent: CRS, min/max X, min/max Y
extent = new itowns.Extent(
    'EPSG:3946',
    1837816.94334, 1847692.32501,
    5170036.4587, 5178412.82698
);
const center = extent.center();

// `viewerDiv` will contain iTowns' rendering area (`<canvas>`)
viewerDiv = document.getElementById('viewerDiv');

// Instanciate PlanarView*
view = new itowns.PlanarView(viewerDiv, extent, { placement: { heading: 49.6, range: 420, tilt: 30 }, noControls: true });

const controls = new OrbitControls(view.camera.camera3D, viewerDiv);
controls.target.set(center.x, center.y, 200);


view._controlFunctions = {
          frameRequester: () => controls.update(),
          eventListener: () => view.notifyChange(view.camera.camera3D),
      };

      if (typeof controls.addEventListener === 'function') {
          controls.addEventListener('change', view._controlFunctions.eventListener);
      // Some THREE controls don't inherit of EventDispatcher
      } else {
          throw new Error(
              'Unsupported control class: only event dispatcher controls are supported.',
          );
      }

      view.addFrameRequester('before_camera_update', view._controlFunctions.frameRequester);



// Add a WMS imagery source
var wmsImagerySource = new itowns.WMSSource({
    extent: extent,
    name: 'ortho_latest',
    url: 'https://imagerie.data.grandlyon.com/wms/grandlyon',
    version: '1.3.0',
    crs: 'EPSG:3946',
    format: 'image/jpeg',
});

// Add a WMS imagery layer
var wmsImageryLayer = new itowns.ColorLayer('wms_imagery', {
    updateStrategy: {
        type: itowns.STRATEGY_DICHOTOMY,
        options: {},
    },
    source: wmsImagerySource,
});

view.addLayer(wmsImageryLayer);

// Add a WMS elevation source
var wmsElevationSource = new itowns.WMSSource({
    extent: extent,
    url: 'https://imagerie.data.grandlyon.com/wms/grandlyon',
    name: 'MNT2018_Altitude_2m',
    crs: 'EPSG:3946',
    width: 256,
    format: 'image/jpeg',
});

// Add a WMS elevation layer
var wmsElevationLayer = new itowns.ElevationLayer('wms_elevation', {
    useColorTextureElevation: true,
    colorTextureElevationMinZ: 144,
    colorTextureElevationMaxZ: 622,
    source: wmsElevationSource,
});

view.addLayer(wmsElevationLayer);

var wmsOmbrageSource = new itowns.WMSSource({
    extent: extent,
    url: 'https://imagerie.data.grandlyon.com/wms/grandlyon',
    name: 'MNT2009_ombrage_10m_CC46',
    crs: 'EPSG:3946',
    width: 256,
    format: 'image/jpeg',
});

// Add a WMS elevation layer
var wmsOmbrageLayer = new itowns.ColorLayer('MNT2009_ombrage_10m_CC46', {
    useColorTextureElevation: true,
    colorTextureElevationMinZ: 144,
    colorTextureElevationMaxZ: 622,
    source: wmsOmbrageSource,
});
wmsOmbrageLayer.opacity = 0.3;

view.addLayer(wmsOmbrageLayer);



var wfsCartoSource = new itowns.WFSSource({
    url: 'https://wxs.ign.fr/cartovecto/geoportail/wfs?',
    version: '2.0.0',
    typeName: 'BDCARTO_BDD_WLD_WGS84G:zone_habitat_mairie',
    crs: 'EPSG:3946',
    ipr: 'IGN',
    format: 'application/json',
});

var wfsCartoStyle = {
    zoom: { min: 0, max: 20 },
    text: {
        field: '{toponyme}',
        color: 'white',
        transform: 'uppercase',
        size: 15,
        haloColor: 'rgba(20,20,20, 0.8)',
        haloWidth: 3,
    },
};

var wfsCartoLayer = new itowns.LabelLayer('wfsCarto', {
    source: wfsCartoSource,
    style: wfsCartoStyle,
});

view.addLayer(wfsCartoLayer);

const zAxis = new THREE.Vector3(0, 0, 1);
// typically, up is z in GIS context
THREE.Object3D.DEFAULT_UP = zAxis;


const scene = view.scene;
const group = new THREE.Group();
group.position.set(center.x, center.y, 200);
group.updateMatrixWorld();
scene.add(group);


// renderer
view.mainLoop.gfxEngine.renderer.shadowMap.enabled = true;

// White directional light at half intensity shining from the top.
const directionalLight = new THREE.DirectionalLight( 0xffffff, 1 );
directionalLight.position.set(0, 10, 70);
directionalLight.target.position.set(0, 0, -10);
directionalLight.castShadow = true;
directionalLight.shadow.camera.left = 50;
directionalLight.shadow.camera.right = -50;
directionalLight.shadow.camera.top = 50;
directionalLight.shadow.camera.bottom = -50;
directionalLight.shadow.mapSize.width = 2048;
directionalLight.shadow.mapSize.height = 2048;

directionalLight.updateMatrixWorld();
scene.add( directionalLight );
const ambientLight = new THREE.AmbientLight(0xffffff, 0.4);
scene.add(ambientLight);

// add a skybox background
const cubeTextureLoader = new THREE.CubeTextureLoader();
cubeTextureLoader.setPath('./skyboxsun25deg_zup/');
const cubeTexture = cubeTextureLoader.load([
    'px.jpg', 'nx.jpg',
    'py.jpg', 'ny.jpg',
    'pz.jpg', 'nz.jpg',
]);

scene.background = cubeTexture;

// array of obj spec
const objects = [
  {
    geom: new THREE.BoxGeometry(1, 1, 1),
    mat: new THREE.MeshLambertMaterial({ color: 0x00FF00 }),
    position: [0 ,0, 5],
    rotationSpeed: new THREE.Vector2(0.01, 0.01),
    castShadow: true,
    receiveShadow: true
  },
  {
    geom: new THREE.BoxGeometry(1, 2, 1),
    mat: new THREE.MeshLambertMaterial({ color: 0x4925ff }),
    position: [10, 10, 0],
    castShadow: true,
    receiveShadow: true
  },
  {
    geom: new THREE.PlaneGeometry(100, 100),
    mat: new THREE.MeshLambertMaterial({ color: 0xaaaaaa }),
    useTexture: true,
    receiveShadow: true
  },
  {
    geom: new THREE.CapsuleGeometry(3, 10, 30, 30),
    mat: new THREE.MeshLambertMaterial({ color: 0xffFF00 }),
    rotationSpeed: new THREE.Vector2(0.001, 0.001),
    position: [0, 20, 8],
    castShadow: true,
    receiveShadow: true
  },
  {
    geom: new THREE.CircleGeometry( 5, 32 ),
    mat: new THREE.MeshLambertMaterial({ color: 0xc561ff }),
    rotationSpeed: new THREE.Vector2(0.001, 0.001),
    position: [0, 20, 8],
    castShadow: true,
    receiveShadow: true

  },
  {
    geom: new THREE.ConeGeometry( 5, 20, 32 ),
    mat: new THREE.MeshLambertMaterial({ color: 0xb0ffd8 }),
    position: [20, 20, 11],
    rotation: [Math.PI / 2, 0, 0],
    castShadow: true,
    receiveShadow: true
  },
  {
    geom: new THREE.SphereGeometry(5, 20, 32),
    mat: new THREE.MeshLambertMaterial({ color: 0xb0ffd8 }),
    position: [-10, -10, 8],
    castShadow: true,
    receiveShadow: true
  },
  {
    geom: new THREE.BoxGeometry(2, 2),
    mat: new THREE.MeshLambertMaterial({ color: 0xb0d8 }),
    position: [-10, -20, 8],
    rotation: [Math.PI / 2, 0, 0],
    opacity: 0.5,
    castShadow: true,
    receiveShadow: true
  },
];

const texture = new THREE.TextureLoader().load('./mario.png' );

// build objects
for (const obj of objects) {
  const mesh = new THREE.Mesh(obj.geom, obj.mat);
  if (obj.useTexture) {
    mesh.material.map = texture;
  }

  mesh.userData.rotationSpeed = obj.rotationSpeed;
  if (obj.position) {
    mesh.position.fromArray(obj.position);
  }
  if (obj.rotation) {
    mesh.rotation.set(obj.rotation[0], obj.rotation[1], obj.rotation[2]);
  }
  if (obj.opacity) {
    mesh.material.opacity = obj.opacity;
    if (obj.opacity < 1.0) {
      mesh.material.transparent = true;
      mesh.material.side = THREE.DoubleSide;
    }
  }
  mesh.userData.color = mesh.material.color.clone();
  // shadows
  mesh.receiveShadow = obj.receiveShadow;
  mesh.castShadow = obj.castShadow;
  group.add(mesh);
  mesh.updateMatrixWorld();
  view.notifyChange();
  obj.mesh = mesh;
}

// load obj
// instantiate a loader
const mtlLoader = new MTLLoader();
mtlLoader.load('./house.mtl', (mtl) => {
  mtl.preload();
  const loader = new OBJLoader();
  loader.setMaterials(mtl);
  // load a resource
  loader.load(
    // resource URL
    './house.obj',
    // called when resource is loaded
        function ( object ) {
            object.rotation.x = Math.PI / 2;
            object.position.x = 100;
            object.traverse(s => {
                if (s.material && s.material.color) {
                    s.userData.color = s.material.color.clone();
                }
                // for some reason this mesh has lines instead of meshes sometimes
                if (s.isLine) {
                    s.visible = false;
                    const mesh = new THREE.Mesh(s.geometry);
                    mesh.material = s.material.map(mat => {
                        const m = new THREE.MeshLambertMaterial({ color: mat.color})
                        if (m.color.r < 0.05 && m.color.g < 0.05 && m.color.b < 0.05) {
                            m.color.set(0xcccccc);
                        }
                        return m;
                    });
                    object.add(mesh);
                }
            });
            group.add( object );
            object.updateMatrixWorld();
            view.notifyChange();

            window.house = object;
        }
    );
});
// tidal locked orbit
const earth = objects[6].mesh;
const moon = objects[7].mesh;

const distance = earth.position.distanceTo(moon.position);

// picking
const raycaster = new THREE.Raycaster();
let pointer;

function onPointerMove( event ) {

    // calculate pointer position in normalized device coordinates
    // (-1 to +1) for both components
    if (!pointer) {
        pointer = new THREE.Vector2();
    }

    pointer.x = ( event.clientX / window.innerWidth ) * 2 - 1;
    pointer.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

}
function animate() {
  const t = Date.now();

  // animate objects
  group.traverse(o => {
    if (o.userData.rotationSpeed) {
      o.rotation.x += o.userData.rotationSpeed.x;
      o.rotation.y += o.userData.rotationSpeed.y;
    }

  });
  const angleFrac = (t / 300) % (Math.PI * 2);
  const newPosition = earth.position.clone();
  newPosition.add((new THREE.Vector3(distance, 0, 0)).applyAxisAngle(zAxis, angleFrac));
  moon.position.copy(newPosition);
  moon.rotation.y = angleFrac;
  moon.updateMatrixWorld();

  // update the picking ray with the camera and pointer position
  if (pointer) {
    raycaster.setFromCamera( pointer, view.camera.camera3D );

    // calculate objects intersecting the picking ray
    const intersects = raycaster.intersectObjects( group.children );
    group.traverse(o => {
      if (o.material && o.material.color) {
        if (intersects.find(i => i.object == o)) {
          o.material.color.set(0x08ff08);
        } else {
          o.material.color.copy(o.userData.color);
        }
        o.material.needsUpdate = true;
      }
    });
  }
    view.notifyChange();

}
view.addFrameRequester(itowns.MAIN_LOOP_EVENTS.BEFORE_CAMERA_UPDATE, animate);
window.addEventListener( 'pointermove', onPointerMove );

// controls

// CUBE

function calculateCurrentOrientation(view) {
  const camera = view.camera.camera3D;
  const orientation = {};
  const q = new THREE.Quaternion().setFromUnitVectors(
      new THREE.Vector3(0, 1, 0), camera.up);
  q.invert();
  // compute r
  const r = camera.quaternion.clone().premultiply(q);
  // tranform it to euler
  const e = new THREE.Euler(0, 0, 0, 'YXZ').setFromQuaternion(r);
  orientation.phi = THREE.MathUtils.radToDeg(e.x);
  orientation.theta = THREE.MathUtils.radToDeg(-e.y);
  return orientation;
}

let theta = 0;
let phi = 0;
let mouseMoving = false;
let mouseDown = false;

const cube = document.getElementById("cube");
const cubeWidget = document.getElementById("cube_widget");
const cubeFaces = document.querySelectorAll(".cube__face");

// the next two are just used to detect move to avoid click event in these cases
cube.addEventListener("mousedown", (e) => {
  e.preventDefault();
  mouseMoving = false;
  mouseDown = true;
});
cube.addEventListener("mouseup", (e) => {
  e.preventDefault();
  mouseDown = false;
});

cubeWidget.addEventListener("mousemove", e => {
  mouseMoving = true;
  if (e.buttons === 1) {
    e.preventDefault();
    theta += e.movementX;
    phi += -e.movementY;
    cube.style.setProperty("--theta", theta + "deg");
    cube.style.setProperty("--phi", phi + "deg");
  } else {
    mouseDown = false;
  }
}, true);

function markSelected(cubeFace) {
  for (const cf of cubeFaces) {
    if (cf === cubeFace) {
      cf.classList.add('selected');
    } else {
      cf.classList.remove('selected');
    }
  }
}

view.addFrameRequester(itowns.MAIN_LOOP_EVENTS.AFTER_CAMERA_UPDATE, () => {
    const position = view.camera.camera3D.position;
    const {theta, phi} = calculateCurrentOrientation(view);
    cube.style.setProperty("--theta", theta + "deg");
    cube.style.setProperty("--phi", phi + "deg");

});

const tmpVec3 = new THREE.Vector3();
for (const cubeFace of cubeFaces) {
  cubeFace.addEventListener("click", e => {
    if (!mouseMoving) {
      markSelected(e.target);
      const style = getComputedStyle(e.target);
      const m = style.getPropertyValue("--m");
      const i = style.getPropertyValue("--i");
      theta = -(1 - i) * m * 90;
      phi = -i * m * 90;
      cube.style.setProperty("--theta", theta + "deg");
      cube.style.setProperty("--phi", phi + "deg");

      // place camera
      tmpVec3.copy(group.position);
      const distance = tmpVec3.distanceTo(view.camera.camera3D.position);
      let axe;
      switch (e.target.id) {
        case 'south':
          axe = new THREE.Vector3(0, -1, 0);
          break;
        case 'north':
          axe = new THREE.Vector3(0, 1, 0);
          break;
        case 'east':
          axe = new THREE.Vector3(1, 0, 0);
          break;
        case 'west':
          axe = new THREE.Vector3(-1, 0, 0);
          break;
        case 'top':
          axe = new THREE.Vector3(0, 0, 1);
          break;
        case 'bottom':
          axe = new THREE.Vector3(0, 0, -1);
          break;
        default:
          throw new Error(`invalid action ${e.target.id}`);
      }
      tmpVec3.addScaledVector(axe, distance);
      view.camera.camera3D.position.copy(tmpVec3);
      view.notifyChange();
      controls.update();
    }
  });
}
