# Workshop iTowns

## Getting started

- copier `main_clean.js` à la place de `main.js`
- installer les dépendances: `npm i`
- démarrer le serveur: `npm start`

## Contexte SIG

- instanciation de la vue (planaire)
- ajout d'un fond de plan
- ajout d'une élévation
- ajouter un deuxième fond de plan et jouer sur l'opacité des fonds de plan. Possibilité: un autre fond de plan du grand lyon: https://imagerie.data.grandlyon.com/wms/grandlyon?request=GetCapabilities layer "MNT2009_ombrage_10m_CC46"

## utiliser three.js avec iTowns

- Récupérer le chargement obj et les fonctionnalités implémentées précédemment. Quel est le moyen le plus facile pour tout déplacer ?
- Ajouter un moyen d'orbiter un objet
- optionnel: limiter les déplacements de la caméra pour ne pas trop s'éloigner
- implémenter des vues est-ouest-nord-sud sur un objet
    - récupérer l'objet (picking ou graphe de scène)

## Enrichir la scène

- Ajouter un "gizmo 3D" pour donner l'orientation:
    - solution 1: [ce cube css](https://codepen.io/autra/pen/PVBKVq)
    - solution 2: une deuxième scène three.js avec un [AxesHelper](https://threejs.org/docs/?q=Ax#api/en/helpers/AxesHelper). Ajouter des labels sur les axes.
- Ajouter une couche de label en utilisant une source et `LabelLayer`

## Couche 3dtiles

- intégrer une couche 3dtiles
- mettre en évidence des objets au survol avec l'opacité

Autres idées:

- flux WFS?
- flux 3dtiles?
- Votre cas d'usage ?


# Note:
    
## Accéder aux objets THREE
    
- renderer: `view.mainLoop.gfxEngine.renderer`
- camera: `view.camera.camera3D`
    
## Se brancher à la boucle de rendu
    
```js
view.addFrameRequester(itowns.MAIN_LOOP_EVENTS.AFTER_CAMERA_UPDATE, () => {
    const position = view.camera.camera3D.position;
    /* ... */

});
```

## Intégration d'un contrôle three
    
```js
      view._controlFunctions = {
          frameRequester: () => controls.update(),
          eventListener: () => view.notifyChange(view.camera.camera3D),
      };

      if (typeof controls.addEventListener === 'function') {
          controls.addEventListener('change', view._controlFunctions.eventListener);
      // Some THREE controls don't inherit of EventDispatcher
      } else {
          throw new Error(
              'Unsupported control class: only event dispatcher controls are supported.',
          );
      }

      view.addFrameRequester('before_camera_update', view._controlFunctions.frameRequester);

```
## Content

The workshop is based on the following technologies:

* [iTowns2](https://github.com/iTowns/itowns): a 3D web client to display 3D data in a browser, based on WebGL, Three.js and PoTree

## Troubleshooting

You can report issues with this workshop on this GitHub's repository issue tracker.

---

Shield: [![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]

This work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg
